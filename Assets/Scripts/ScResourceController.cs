﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScResourceController : MonoBehaviour
{
    public Button _resourceButton;
    public Image _resourceImage;
    public Text _resourceDescription;
    public Text _resourceUpgradeCost;
    public Text _resourceUnlockCost;

    private int _level = 1;

    public bool IsUnlocked { get; private set; } //

    private ResourceConfig _config;

    private void Start()
    {
        _resourceButton.onClick.AddListener(() =>
        {
            if (IsUnlocked)
            {
                UpgradeLevel();
            }
            else
            {
                UnlockResource();
            }
        });
    }

    private void UpgradeLevel()
    {
        double upgradeCost = GetUpgradeCost();
        if (ScGameManager.Instance.TotalGold < upgradeCost)
        {
            return;
        }
        ScGameManager.Instance.AddGold(-upgradeCost);
        _level++;

        _resourceUpgradeCost.text = $"Upgrade Cost\n{GetUpgradeCost()}";
        _resourceDescription.text = $"{_config.name} Lv.{_level}\n+{GetOutput().ToString("0")}";
    }

    private void UnlockResource()
    {
        double unlockCost = GetUnlockCost();
        if (ScGameManager.Instance.TotalGold < unlockCost)
        {
            return;
        }
        SetUnlocked(true);
        ScGameManager.Instance.ShowNextResource(); // kalau sudah ke unlock maka resource selanjutnya dimunculkan untuk diunlock
        ScAchievementController.Instance.UnlockAchievement(ScAchievementController.AchievementType.UnlockResource,_config.name);
    }

    public void SetUnlocked(bool unlocked)
    {
        IsUnlocked = unlocked;
        _resourceImage.color = IsUnlocked ? Color.white : Color.grey;
        _resourceUnlockCost.gameObject.SetActive(!unlocked);
        _resourceUpgradeCost.gameObject.SetActive(unlocked);

    }


    //SET TEXT
    public void SetConfig(ResourceConfig config)
    {
        _config = config;

        _resourceDescription.text = $"{_config.name} Lv. {_level} \n+{GetOutput().ToString("0")}";
        _resourceUnlockCost.text = $"Unlock Cost\n{_config.unlockCost}";
        _resourceUpgradeCost.text = $"Upgrade Cost\n{GetUpgradeCost()}";

        SetUnlocked(_config.unlockCost == 0);
        //jika unlock cost 0 => langsung terbuka
    }

    public double GetOutput()
    {
        return _config.output * _level;
    }

    public double GetUpgradeCost()
    {
        return _config.upgradeCost * _level;
    }

    public double GetUnlockCost()
    {
        return _config.unlockCost;
    }

}
